#!/bin/sh

# Create /public directory
rm -rf public
mkdir -p public/

# Generate tokens
yarn install --frozen-lockfile
yarn build

# Build dashboard
cd dashboard
yarn install --frozen-lockfile
yarn generate
cd ..

mv dashboard/dist/* public
