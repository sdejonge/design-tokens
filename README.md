# Design tokens

A versionable, structured, source-of-truth for foundational design tokens (colors, size scales etc.) using [style-dictionary](https://amzn.github.io/style-dictionary) to generate variables from globally defined design tokens.

## Getting started

Generate tokens into consumable variables (`.scss`, `.css`, `.js` etc.)

```bash
yarn && yarn tokens
```

Build dashboard 

```bash
cd dashboard
yarn && yarn dev
```

View dashboard at http://localhost:8000/ 

## What are design tokens?

> Design tokens are a methodology for expressing design decisions in a platform-agnostic way so that they can be shared across different disciplines, tools, and technologies. They help establish a common vocabulary across organisations.

From: [Design Tokens Format Module](https://tr.designtokens.org/format/)

## Values

### Basic values

Basic values are represented in a string, for example a color (`#1f75cb`), font weight (`bold`), spacing scale (`1rem`), or motion easing/duration (`ease-out`, `0.2s`).

```json
{
  "token": { "value": "1rem" }
}
```

### Value categories

A category can be used to transform values to meaningful outputs for each platform, in style-dictionary there are default categories for [color](https://amzn.github.io/style-dictionary/#/tokens?id=category-color), [size](https://amzn.github.io/style-dictionary/#/tokens?id=category-size), [time](https://amzn.github.io/style-dictionary/#/tokens?id=category-time), [asset](https://amzn.github.io/style-dictionary/#/tokens?id=category-asset), and [content](https://amzn.github.io/style-dictionary/#/tokens?id=category-content).

A category is either interpreted from the token structure when using the [Category / Type / Item (CTI) structure](https://amzn.github.io/style-dictionary/#/tokens?id=category-type-item) or by explicitly defining a category in the token `attributes` property e.g.

```json
{
  "size": { "value": "16", "attributes": { "category": "size" } },
  "time": { "value": "500", "attributes": { "category": "time" } },
}
```

Will be transformed into the following CSS/SCSS output when the [`size/pxToRem`](https://amzn.github.io/style-dictionary/#/transforms?id=sizerem), [`time/seconds`](https://amzn.github.io/style-dictionary/#/transforms?id=timeseconds) transforms used.

**CSS:**

```css
:root {
  --size: 1rem;
  --time: 0.5s;
}
```

**SCSS:**

```scss
$size: 1rem;
$time: 0.5s;
```


### Reference values

Reference values associate one token's value with another as an alias, for example the alias token `blue.500` has the value `{gl.color.blue.500}`.

```json
{
  "blue-500": { "value": "{gl.color.blue.500}" }
}
```

This allows output CSS, SCSS to use references as variables:

**CSS:**

```css
:root {
  --blue-500: var(--gl-color-blue-500);
}
```

**SCSS:**

```scss
$blue-500: $gl-color-blue-500;
```

### Composite values

Composite values can be used for complex tokens for example a shadow `0px 1px 2px rgba(31, 30, 36, 0.1)` can be represented as:

```json
{
  "shadow": {
    "value": {
      "x": 0,
      "y": 1,
      "blur": 2,
      "spread": 0,
      "color": "rgba(31, 30, 36, 0.1)"
    }
  },
}
```

The color can also be broken down further to an reference `{gl.color.gray.950}` and opacity value:

```json
{
  "shadow": {
    "value": {
      "x": 0,
      "y": 1,
      "blur": 2,
      "spread": 0,
      "color": "{gl.color.gray.950}",
      "opacity": 0.1
    }
  },
}
```

**Note:** [style-dictionary requires transforms](https://github.com/amzn/style-dictionary/issues/848#issuecomment-1257275868) for composite values to resolve into meaningful strings. Otherwise output will be `[Object object]`.

## Base tokens

A base token is a name and value pairing that defines a design system primitive (e.g. `$gl-color-blue-500`)

Consolidate token names to a consistent convention and provide aliases for existing variable usage to be deprecated:

```
{namespace}-{context}-{property}-{value}-{scale}-{variant}
```

- `namespace` global namespace constraint e.g. `gl`
- `context` (optional) conceptual area e.g. `text` or `theme`
- `property` (optional) style property e.g. `border-radius` or `font-weight`
- `value` (optional) token name e.g. `blue` or `bold`
- `scale` (optional) token scale e.g. `500` or `small` 
- `variant` (optional) token variant e.g. `alpha-008` or `lighter`

<details><summary>Examples</summary>

Put together these can be chained to replicate existing variable usage:

| Example                    | Namespace | Context  | Property      | Value        | Scale | Variant       |
| -------------------------- | --------- | -------- | ------------- | ------------ | ----- | ------------- |
| $anchor-color              |           | anchor   | color         |              |       |               |
| $base-font-size            |           | base     | font-size     |              |       |               |
| $blue-500                  |           |          |               | blue         | 500   |               |
| $border-color              |           |          | border-color  |              |       |               |
| $border-gray-light         |           |          | border        | gray         |       | light         |
| $border-gray-normal        |           |          | border        | gray         |       | normal        |
| $border-gray-normal-dashed |           |          | border        | gray         |       | normal-dashed |
| $border-radius             |           |          | border-radius |              |       |               |
| $border-width              |           |          | border-width  |              |       |               |
| $breakpoint-md             |           |          | breakpoint    |              | md    |               |
| $data-viz-blue-500         |           | data-viz |               | blue         | 500   |               |
| $font-family-sans          |           |          | font-family   | sans         |       |               |
| $gitlab-logo-grey          | gitlab    | logo     |               | grey         |       |               |
| $gl-border-radius-small    | gl        |          | border-radius |              | small |               |
| $gl-border-size-3          | gl        |          | border-size   |              | 3     |               |
| $gl-font-weight-bold       | gl        |          | font-weight   |              |       | bold          |
| $gl-line-height-32         | gl        |          | line-height   |              | 32    |               |
| $gl-regular-font           | gl        |          |               | regular-font |       |               |
| $gl-spacing-1              | gl        |          | spacing       |              | 1     |               |
| $gl-spacing-11-5           | gl        |          | spacing       |              | 11-5  |               |
| $gray-lightest             |           |          |               | gray         |       | lightest      |
| $gray-normal               |           |          |               | gray         |       | normal        |
| $indigo-900-alpha-008      |           |          |               | indigo       | 900   | alpha-008     |
| $lg-font-size              |           | lg       | font-size     |              |       |               |
| $regular-font              |           |          |               | regular-font |       |               |
| $t-blue-500                |           | t        |               | blue         | 500   |               |
| $t-blue-a-02               |           | t        |               | blue         |       | a-02          |
| $t-gray-a-02               |           | t        |               | gray         |       | a-02          |
| $tanuki-orange             |           | tanuki   |               | orange       |       |               |
| $tanuki-red                |           | tanuki   |               | red          |       |               |
| $tanuki-yellow             |           | tanuki   |               | yellow       |       |               |
| $text-color                |           | text     | color         |              |       |               |
| $text-color--secondary     |           | text     | color         |              |       | secondary     |
| $theme-light-blue-500      |           | theme    |               | light-blue   | 500   |               |
| $white                     |           |          |               | white        |       |               |
| $white-contrast            |           |          |               | white        |       | contrast      |

</details>

## Design tokens

A design token represents a design decision (e.g `gl.color.anchor`) and can use either a value directly or alias a base token

| Design token              | Base token            | Light     | Dark      | CSS                         | SCSS                       |
| ------------------------- | --------------------- | --------- | --------- | --------------------------- | -------------------------- |
| `gl.color.text`           | `gl.color.gray.900`   | `#333238` | `#ececef` | `--gl-color-text`           | `$gl-color-text`           |
| `gl.color.text.secondary` | `gl.color.gray.500`   | `#737278` | `#89888d` | `--gl-color-text-secondary` | `$gl-color-text-secondary` |
| `gl.color.success`        | `gl.color.green.500`  | `#108548` | `#2da160` | `--gl-color-success`        | `$gl-color-success`        |
| `gl.color.info`           | `gl.color.blue.500`   | `#1f75cb` | `#428fdc` | `--gl-color-info`           | `$gl-color-info`           |
| `gl.color.warning`        | `gl.color.orange.500` | `#ab6100` | `#c17d10` | `--gl-color-warning`        | `$gl-color-warning`        |
| `gl.color.danger`         | `gl.color.red.500`    | `#dd2b0e` | `#ec5941` | `--gl-color-danger`         | `$gl-color-danger`         |

#### Design token variants

Design tokens may define variants in the case of muted, accent, emphasis etc.

| Design token                        | Base token           | Light     | Dark      | CSS                                   | SCSS                                 |
| ----------------------------------- | -------------------- | --------- | --------- | ------------------------------------- | ------------------------------------ |
| `gl.color.success`                  | `gl.color.green.500` | `#108548` | `#2da160` | `--gl-color-success`                  | `$gl-color-success`                  |
| `gl.color.success.muted.background` | `gl.color.green.100` | `#c3e6cd` | `#0d532a` | `--gl-color-success-muted-background` | `$gl-color-success-muted-background` |
| `gl.color.success.muted.foreground` | `gl.color.green.700` | `#24663b` | `#91d4a8` | `--gl-color-success-muted-foreground` | `$gl-color-success-muted-foreground` |

## Aliases

Aliases refer to base token or design token aliases to maintain existing variables names in the codebase during migration

| Token               | Alias      | Alias CSS                         | Alias SCSS                      |
| ------------------- | ---------- | --------------------------------- | ------------------------------- |
| `gl.color.blue.500` | `blue.500` | `--blue-500: --gl-color-blue-500` | `$blue-500: $gl-color-blue-500` |

## Modes

Modes provide a way to change values for groups of tokens. Examples of different modes could include:

- **Dark:** overrides of base `color` tokens for dark mode
- **Compact:** overrides of base `spacing` tokens for more compact UI
- **High-contrast:** overrides of base `color` tokens, and/or text and border design tokens
- **Tritanopia, Protanopia & Deuteranopia:** overrides of base `color` tokens to maintain color contexts for people with color blindness
- **Reduced motion:** overrides of base tokens like `duration` to reduce motion

Modes are processed on top of default tokens and can be combined with other modes, and inherited separately from stylesheets for example:

`color.base.json`:

```json
{
  "text": "#000000",
  "blue": "#0000ff"
}
```

`color.dark.json`:

```json
{
  "text": "#ffffff"
}
```

Will generate `tokens.css` and `tokens.dark.css` where only the updated values are included:

`tokens.css`:

```css
:root {
  --text: #000000;
  --blue: #0000ff;
}
```

`tokens.dark.css`:

```css
:root {
  --text: #ffffff;
}
```

## Examples

### `gray-900`

<div class="center">
  
```mermaid
flowchart TB
  Root[color] --> |Value| Value([#333238])
  Value --> |Base token| BaseToken(gl.color.gray.900)
  BaseToken --> |SCSS| BaseTokenSCSS([$gl-color-gray-900])
  BaseTokenSCSS --> |Aliases| Aliases([$gray-900])
  BaseToken --> |Design token| DesignToken(gl.color.text)
  DesignToken --> |Figma| DesignTokenFigma([color.text])
  DesignToken --> |CSS| DesignTokenCSS([--gl-color-text])
  DesignToken --> |SCSS| DesignTokenSCSS([$gl-color-text])
  DesignTokenSCSS --> |Aliases| DesignTokenAliases([$text-color])
```
  
</div>

### `gray-500`

<div class="center">
  
```mermaid
graph TD
  Root[color] --> |Value| Value([#737278])
  Value --> |Base token| BaseToken(gl.color.gray.500)
  BaseToken --> |SCSS| BaseTokenSCSS([$gl-color-gray-500])
  BaseTokenSCSS --> |Aliases| Aliases([$gray-500])
  BaseToken --> |Design token| DesignToken(gl.color.text.secondary)
  DesignToken --> |Figma| DesignTokenFigma([color.text.secondary])
  DesignToken --> |CSS| DesignTokenCSS([--gl-color-text-secondary])
  DesignToken --> |SCSS| DesignTokenSCSS([$gl-color-text-secondary])
  DesignTokenSCSS --> |Aliases| DesignTokenAliases([$text-color-secondary])
```
  
</div>

### `blue-500`

<div class="center">
  
```mermaid
flowchart TB
  Root[color] --> |Value| Value([#1f75cb])
  Value --> |Base token| BaseToken(gl.color.blue.500)
  BaseToken --> |SCSS| BaseTokenSCSS([$gl-color-blue-500])
  BaseTokenSCSS --> |Aliases| Aliases([$blue-500])
  BaseToken --> DesignTokenBackground(gl.color.background.confirm)
  BaseToken --> DesignTokenLink(gl.color.link)
  BaseToken --> DesignTokenButtonBackground(gl.color.button.confirm.background)
  subgraph gl.background.confirm
    DesignTokenBackground --> |Figma| DesignTokenBackgroundFigma([color.background.confirm])
    DesignTokenBackground --> |CSS| DesignTokenBackgroundCSS([--gl-color-background-confirm])
    DesignTokenBackground --> |SCSS| DesignTokenBackgroundSCSS([$gl-color-background-confirm])
    DesignTokenBackgroundSCSS --> |Aliases| DesignTokenBackgroundAliases([$text-color])
  end
  subgraph gl.color.link
    DesignTokenLink --> |Figma| DesignTokenLinkFigma([color.link])
    DesignTokenLink --> |CSS| DesignTokenLinkCSS([--gl-color-link])
    DesignTokenLink --> |SCSS| DesignTokenLinkSCSS([$gl-color-link])
    DesignTokenLinkSCSS --> |Aliases| DesignTokenLinkAliases([$anchor-color])
  end
  subgraph gl.color.button.confirm.background
    DesignTokenButtonBackground --> |Figma| DesignTokenButtonBackgroundFigma([color.button.confirm.background])
    DesignTokenButtonBackground --> |CSS| DesignTokenButtonBackgroundCSS([--gl-color-button-confirm-background])
    DesignTokenButtonBackground --> |SCSS| DesignTokenButtonBackgroundSCSS([$gl-color-button-confirm-background])
  end
```
  
</div>

## Design Tokens Format Module

The [Design Tokens Format Module](https://tr.designtokens.org/format/) is a [community group draft report](https://www.w3.org/standards/types#reports), published by the [Design Tokens Community Group](https://www.w3.org/community/design-tokens/), that describes the technical specification for a file format to exchange design tokens between different tools.

The Design Tokens Format Module promotes a `*.token.json` extension standard for design token files, with a format that includes `$value` and an explicit `$type`:

```json
{
  "token name": {
    "$value": "#ff0000",
    "$type": "color"
  }
}
```

### Design Tokens Format Module vs. Style dictionary

```diff
 {
   "token name": {
-    "$value": "#fff000",
+    "value": "#fff000",
-    "$type": "color",
-    "$description": "..."
+    "comment": "..."
   }
 }
```

#### Value

- **Design Tokens Format Module:** expects a token to have a `$value` property
- **Style dictionary:** expects a token to have a `value` property

#### Type

- **Design Tokens Format Module:** uses an explicit `$type` property
- **Style dictionary:** uses a `category → type → item` (CTI) convention to identify design tokens. This encodes information about the token implicitly into the nesting

#### Description

- **Design Tokens Format Module:** uses a `$description` property to output comments in some formats
- **Style dictionary:** uses a `comment` property to output comments in some formats
