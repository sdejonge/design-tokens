/* eslint-disable no-console */
const StyleDictionary = require('style-dictionary');

const cssTransforms = [
  'attribute/cti',
  'color/hex',
  'name/cti/kebab',
  'size/pxToRem',
  'time/seconds',
];

const jsTransforms = [
  'attribute/cti',
  'color/hex',
  'name/cti/constant',
  'size/pxToRem',
  'time/seconds',
];

const modes = [
  'dark',
  'compact',
];

/**
 * Creates destination filename from options
 *
 * @param {Object} [options]
 * @param {String} [options.name] name e.g. tokens
 * @param {String} [options.mode] mode e.g. dark
 * @param {String} [options.extension] file extension e.g. .scss
 * @returns {String} destination filename e.g. tokens.dark.json
 */
const getDestination = ({ name = 'tokens', mode, extension = 'json' }) => {
  return [
    name,
    mode,
    extension,
  ].filter(Boolean).join('.');
};

/**
 * Creates style-dictionary config by mode by matching token files in
 * `/tokens` directory by filename e.g. `color.base.dark.json` will
 * only generate tokens for dark mode.
 *
 * @param {String} mode for source and destination filenames
 * @param {Function} filter get only matching token files
 * @returns {Object} style-dictionary config
 */
const getStyleDictionaryConfig = (mode, filter) => {
  return {
    include: [`tokens/**/!(*.${modes.join(`|*.`)}).json`],
    source: [`tokens/**/*.${mode}.json`],
    platforms: {
      dashboard: {
        buildPath: 'dashboard/static/tokens/',
        transforms: jsTransforms,
        files: [
          {
            destination: getDestination({ mode }),
            format: 'json',
          },
        ],
      },
      css: {
        buildPath: 'dist/css/',
        transforms: cssTransforms,
        files: [
          {
            destination: getDestination({ mode, extension: 'css' }),
            format: 'css/variables',
            filter,
            options: {
              outputReferences: true,
            },
          },
        ],
      },
      js: {
        buildPath: 'dist/js/',
        transforms: jsTransforms,
        files: [
          {
            destination: getDestination({ mode, extension: 'js' }),
            format: 'javascript/module',
            filter,
          },
          {
            destination:  getDestination({ mode, extension: 'es6.js' }),
            format: 'javascript/es6',
            filter,
          },
        ],
      },
      json: {
        buildPath: 'dist/json/',
        transforms: jsTransforms,
        files: [
          {
            destination: getDestination({ mode }),
            format: 'json',
          },
        ],
      },
      scss: {
        buildPath: 'dist/scss/',
        transformGroup: 'scss',
        transforms: cssTransforms,
        files: [
          {
            destination: getDestination({ name: '_tokens', mode, extension: 'scss' }),
            format: 'scss/variables',
            filter,
            options: {
              outputReferences: true,
            },
          },
        ],
      },
    },
  };
}

// Build default tokens from config
console.log('👷 Building default tokens');
StyleDictionary.extend(getStyleDictionaryConfig()).buildAllPlatforms();

// Build tokens for each mode
modes.forEach(mode => {
  console.log(`\n👷 Building ${mode} tokens`);
  const modeFilter = (token) => token.filePath.indexOf(mode) > -1;
  StyleDictionary.extend(getStyleDictionaryConfig(mode, modeFilter)).buildAllPlatforms();
});
