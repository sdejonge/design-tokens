/**
 * Transforms design token original value string
 * Before: '{group.token.value}'
 * After: 'group.token.value'
 *
 * @param {String} token design token
 * @returns {String} transformed original value
 */
export const transformOriginalValue = (token) => {
  const original = token.original?.value;
  if (
    original &&
    original !== token.value &&
    original.includes('{')
  ) {
    return original.slice(1, -1);
  }
  return null;
};

/**
 * Transforms design token to minimal structure e.g.
 * {
 *   name: 'group.token.value',
 *   original: 'original.value',
 *   value: '#ff0000',
 * }
 *
 * @param {Object} token generated design token
 * @returns {Object} fields and slot content for GlTable
 */
export const transformTokenToTableColumns = (token = {}) => {
  return {
    'name': token.path?.join('.'),
    'original': transformOriginalValue(token),
    'value': token.value,
  };
};

/**
 * Transforms generated tokens and transforms into structure to be
 * consumed by GlTable
 *
 * @param {Object} tokens generated design tokens
 * @returns {Array} An array of tokens transformed to table columns
 */
export const transformTokensToTableRows = (tokens = {}) => {
  const arr = []

  Object.keys(tokens).forEach((key) => {
    const token = tokens[key];
    if (token.value) {
      arr.push(transformTokenToTableColumns(token));
    } else {
      arr.push(...transformTokensToTableRows(token));
    }
  })

  return arr
}
