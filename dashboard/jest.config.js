module.exports = {
  clearMocks: true,
  restoreMocks: true,
  moduleFileExtensions: ['js', 'json', 'vue'],
  moduleNameMapper: {
    '^~/(.*)$': '<rootDir>/$1',
  },
  transform: {
    '^.+\\.js$': 'babel-jest',
    '^.+\\.vue$': '@vue/vue2-jest',
  },
  transformIgnorePatterns: ['node_modules/(?!@gitlab/ui)'],
};
